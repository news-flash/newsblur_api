use crate::NewsBlurApi;
use reqwest::Client;
use std::env;
use url::Url;

fn test_setup_env() -> NewsBlurApi {
    dotenv::dotenv().expect("Failed to read .env file");
    let url = "https://newsblur.com";
    let user = env::var("NEWSBLUR_USER").expect("Failed to read NEWSBLUR_USER");
    let pw = env::var("NEWSBLUR_PW").expect("Failed to read NEWSBLUR_PW");

    let url = Url::parse(url).unwrap();
    NewsBlurApi::new(&url, &user, &pw, None)
}

#[tokio::test]
async fn login() {
    let mut api = test_setup_env();
    let client = Client::new();

    let response = api.login(&client).await;
    assert!(response.is_ok());
}

#[tokio::test]
async fn logout() {
    let mut api = test_setup_env();
    let client = Client::new();

    let response = api.login(&client).await;
    assert!(response.is_ok());

    let response = api.logout(&client).await;
    assert!(response.is_ok());
}

#[tokio::test]
async fn search_feed() {
    let mut api = test_setup_env();
    let client = Client::new();

    let response = api.login(&client).await;
    assert!(response.is_ok());

    let response = api
        .search_feed(&client, "www.abc.net.au/news/feed/51120/rss.xml")
        .await;
    assert!(response.is_ok());
}

#[tokio::test]
async fn get_feeds() {
    let mut api = test_setup_env();
    let client = Client::new();

    let response = api.login(&client).await;
    assert!(response.is_ok());

    let response = api.get_feeds(&client).await;
    assert!(response.is_ok());
}

#[tokio::test]
async fn favicons() {
    let mut api = test_setup_env();
    let client = Client::new();

    let response = api.login(&client).await;
    assert!(response.is_ok());

    let response = api.get_feeds(&client).await.unwrap();
    let feeds = &response["feeds"].as_object().unwrap();

    for (i, feed) in feeds.iter().enumerate() {
        if i > 70 {
            return;
        }

        let response = api.favicons(&client, feed.0).await;
        assert!(response.is_ok());
    }
}

#[tokio::test]
async fn get_original_page() {
    let mut api = test_setup_env();
    let client = Client::new();

    let response = api.login(&client).await;
    assert!(response.is_ok());

    let response = api.get_feeds(&client).await.unwrap();
    let feeds = &response["feeds"].as_object().unwrap();

    for (i, feed) in feeds.iter().enumerate() {
        if i > 70 {
            return;
        }

        let response = api.get_original_page(&client, feed.0).await;
        assert!(response.is_ok());
    }
}

#[tokio::test]
async fn refresh_feeds() {
    let mut api = test_setup_env();
    let client = Client::new();

    let response = api.login(&client).await;
    assert!(response.is_ok());

    let response = api.refresh_feeds(&client).await;
    assert!(response.is_ok());
}

#[tokio::test]
async fn get_stories() {
    let mut api = test_setup_env();
    let client = Client::new();

    let response = api.login(&client).await;
    assert!(response.is_ok());

    let response = api.get_feeds(&client).await.unwrap();
    let feeds = &response["feeds"].as_object().unwrap();

    for (i, feed) in feeds.iter().enumerate() {
        if i > 70 {
            return;
        }

        let response = api.get_stories(&client, feed.0, false, 1).await.unwrap();
        assert_ne!(response["stories"], serde_json::Value::Null);

        let response = api.get_stories(&client, feed.0, false, 2).await.unwrap();
        assert_ne!(response["stories"], serde_json::Value::Null);
    }
}

#[tokio::test]
async fn read_stories() {
    let mut api = test_setup_env();
    let client = Client::new();

    let response = api.login(&client).await;
    assert!(response.is_ok());

    let response = api.get_feeds(&client).await.unwrap();
    let feeds = &response["feeds"].as_object().unwrap();

    for (i, feed) in feeds.iter().enumerate() {
        let response = api.get_stories(&client, feed.0, false, 1).await.unwrap();
        let story_hash = &response["stories"][0]["guid_hash"].as_str().unwrap();
        let read = response["stories"][0]["read_status"].as_i64().unwrap();
        let full_hash = format!("{}:{}", feed.0, story_hash);

        if read > 0 {
            api.mark_story_unread(&client, &[&full_hash]).await.unwrap();
            api.mark_stories_read(&client, &[&full_hash]).await.unwrap();
        } else {
            api.mark_stories_read(&client, &[&full_hash]).await.unwrap();
            api.mark_story_unread(&client, &[&full_hash]).await.unwrap();
        }

        if i > 10 {
            return;
        }
    }
}

#[tokio::test]
async fn star_stories() {
    let mut api = test_setup_env();
    let client = Client::new();

    let response = api.login(&client).await;
    assert!(response.is_ok());

    let response = api.get_feeds(&client).await.unwrap();
    let feeds = &response["feeds"].as_object().unwrap();

    let feed = feeds.iter().next().unwrap();
    let response = api.get_stories(&client, feed.0, false, 1).await.unwrap();
    let story_hash = response["stories"][0]["guid_hash"].as_str().unwrap();
    let full_hash = format!("{}:{}", feed.0, story_hash);

    api.mark_story_hash_as_starred(&client, &full_hash)
        .await
        .unwrap();
    api.mark_story_hash_as_unstarred(&client, &full_hash)
        .await
        .unwrap();
}

#[tokio::test]
async fn get_unread_feeds() {
    let mut api = test_setup_env();
    let client = Client::new();

    let response = api.login(&client).await;
    assert!(response.is_ok());

    let response = api.get_unread_story_hashes(&client).await.unwrap();
    let _hashes = &response["unread_feed_story_hashes"];
}

#[tokio::test]
async fn get_stared_feeds() {
    let mut api = test_setup_env();
    let client = Client::new();

    let response = api.login(&client).await;
    assert!(response.is_ok());

    let response = api.get_stared_story_hashes(&client).await.unwrap();
    let _hashes = &response["starred_story_hashes"];
}
