use thiserror::Error;

#[derive(Error, Debug)]
pub enum ApiError {
    #[error("Failed to Parse URL")]
    Url(#[from] url::ParseError),
    #[error("Http request failed")]
    Http(#[from] reqwest::Error),
    #[error("Request failed with message access denied")]
    AccessDenied,
}
