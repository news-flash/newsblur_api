Rust Bindings for the [NewsBlur API](https://www.newsblur.com/api)

This has been written for use with the NewsFlash feed reader, but can be
used with any projects. This library aims to just be a simple implementation
for the expose NewsBlur API calls.
